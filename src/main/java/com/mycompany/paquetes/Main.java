/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.paquetes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author YordyHz
 */
public class Main {

    public static void main(String[] args) {
        int tam, paq = 0, reserva = 30;

        List<Integer> listPaq = new ArrayList<Integer>();

        Scanner ent = new Scanner(System.in);

        try {
            System.out.println("Cantidad de paquetes: ");

            System.out.println("Ingrese el listado de paquetes. 0 para terminar. ");
            do {
                paq = ent.nextInt();
                if (paq != 0) {
                    listPaq.add(paq);
                }
            } while (paq != 0);


            System.out.println("Ingrese tamaño de camion: ");
            tam = ent.nextInt();

            System.out.println("\nTamaño del Camión: " + tam);
            entrada(listPaq);

            int suma = tam - reserva;
            List<Integer> carga = findSum(listPaq, suma);
            if (carga.size() > 0) {
                System.out.print("\nResultado: ");
                System.out.print(carga.toString());
                System.out.print(" -> La suma de los paquetes es " + suma + " , lo que permite dejar las 30 unidades de espacio requeridas.");
            } else {
                System.out.println("\nNo se puede cargar el camión.");
            }


        } catch (Exception ex) {
            System.out.println("Ha ocurrido un error.");
            System.out.println(ex);
        }

    }

    static void entrada(List<Integer> a) {
        System.out.print("Paquetes: ");
        System.out.print(a.toString());
    }

    static List<Integer> findSum(List<Integer> ln, int k) {
        List<Integer> retorno = new ArrayList<Integer>();
        HashSet<Integer> revisados = new HashSet<>();
        for (int n : ln) {
            int buscar = k - n;
            if (revisados.contains(buscar)) {
                retorno.add(buscar);
                retorno.add(n);
                return retorno;
            }
            revisados.add(n);
        }
        return retorno;
    }

}
